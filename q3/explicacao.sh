#!/bin/bash

# Criando variáveis
variavel1="valor1"
variavel2='valor2'
variavel3=valor3
numero=10

# Exibindo o valor das variáveis
echo "Variável 1: $variavel1"
echo "Variável 2: $variavel2"
echo "Variável 3: $variavel3"
echo "Número: $numero"

# Diferença entre solicitar o valor do usuário e receber como parâmetro
echo -e "\nDiferença entre solicitar valor e receber como parâmetro:"
echo "Digite um valor para a variável 'input_var':"
read input_var
echo "Valor digitado pelo usuário: $input_var"

echo "Valor recebido como parâmetro de linha de comando para a variável 'param_var': $1"

# Variáveis automáticas
echo -e "\nVariáveis automáticas:"
echo "Nome do script: $0"
echo "Quantidade de argumentos passados: $#"
echo "Todos os argumentos passados: $@"
echo "Último argumento passado: ${!#}": 
