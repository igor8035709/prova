#!/bin/bash

a="/tmp"
b="/etc"
c="/bin"

count_a=$(ls -1 "$a" | wc -l)
count_b=$(ls -1 "$b" | wc -l)
count_c=$(ls -1 "$c" | wc -l)

echo "Quantidade de arquivos e diretórios em $a: $count_a"
echo "Quantidade de arquivos e diretórios em $b: $count_b"
echo "Quantidade de arquivos e diretórios em $c: $count_c"

