#!/bin/bash

echo "=== Informações do CPU ==="
lscpu

echo -e "\n=== Informações do PCI ==="
lspci

echo -e "\n=== Informações do USB ==="
lsusb

echo -e "\n=== Informações dos Dispositivos de Bloco (Discos) ==="
lsblk

echo -e "\n=== Informações do Hardware em Geral ==="
lshw

echo -e "\n=== Uso do Disco ==="
df -h

