#!/bin/bash

red='\033[0;31m'
green='\033[0;32m'
yellow='\033[0;33m'
blue='\033[0;34m'
reset='\033[0m'

echo -e "${red}O sol brilha no céu azul.${reset}"
sleep $(shuf -i 5-30 -n 1)
echo -e "${green}As nuvens dançam suavemente.${reset}"
sleep $(shuf -i 5-30 -n 1)
echo -e "${yellow}O vento sussurra entre as folhas.${reset}"
sleep $(shuf -i 5-30 -n 1)
echo -e "${blue}O rio flui calmamente pela paisagem.${reset}"
sleep $(shuf -i 5-30 -n 1)
echo -e "${red}A natureza canta sua melodia tranquila.${reset}"

