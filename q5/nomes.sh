#!/bin/bash

echo "Digite 4 nomes para criar diretórios:"
read a
read b
read c
read d

mkdir "$a" "$b" "$c" "$d"

data=$(date +%d-%m-%Y)

echo -e "# $a\n\nData: $data" > "$a/README.md"
echo -e "# $b\n\nData: $data" > "$b/README.md"
echo -e "# $c\n\nData: $data" > "$c/README.md"
echo -e "# $d\n\nData: $data" > "$d/README.md"

echo "Diretórios e arquivos README.md foram criados com sucesso!"

