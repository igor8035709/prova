#!/bin/bash

proximo_domingo=$(date -d "next Sunday" +%Y-%m-%d)
quarta1=$(date -d "$proximo_domingo -3 days" +%Y-%m-%d)
quarta2=$(date -d "$proximo_domingo +1 days" +%Y-%m-%d)

echo "As duas próximas quartas-feiras serão em:"
echo "Quarta-feira 1: $quarta1"
echo "Quarta-feira 2: $quarta2"



